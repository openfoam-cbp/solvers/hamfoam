# hamFoam

An open-source solver for coupled heat and moisture transport in porous media based on OpenFOAM

> **⚠️ Information**
>
> The repository for the solver is moved to GitHub. Future updates will be at:
>
> https://github.com/OpenFOAM-BuildingPhysics/hamFoam
>
> **⚠️ Information**

<img src="https://carmeliet.ethz.ch/research/downloads/coupled-heat-and-moisture-transport-solver-for-openfoam/_jcr_content/par/fullwidthimage/image.imageformat.fullwidth.1496181206.png"  width="400">

More information at the Chair of Building Physics: https://carmeliet.ethz.ch
